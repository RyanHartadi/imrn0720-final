import React, { Component } from 'react';
import Login from './Login'
import Home from './Home'
import About from './About'
import Detail from './Detail'
import 'react-native-gesture-handler';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
export default class App extends Component {
  
  render(){
    
    const Tab = createBottomTabNavigator();
    const Stack = createStackNavigator();
    return(
      <NavigationContainer>
         <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="Login" component={Login} options={{headerShown:false}}/>
            <Stack.Screen name="Home" component={Home} options={{headerShown:false}} />
            <Stack.Screen name="About" component={About} options={{headerShown:false}} />
            <Stack.Screen name="Detail" component={Detail} options={{headerShown:false}} />
         </Stack.Navigator>
      
      </NavigationContainer>
    )
  }
}
